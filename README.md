# Custom toolbox

```GDAL_Docker_with_oracle_support:``` custom docker image of gdal/python environment with Oracle Spatial and Oracle Georaster support

```GDAL_OGR_scripts:``` command lines example to import/export geospatial data using gdal/ogr

```ORACLE_db_management:``` command to manage your multi-tenant Oracle database

```PYTHON_osm_notebooks:``` python notebooks to play with OSM data: geocoding, overpass-API, ...

```QGIS_build_with_oracle_support:``` step by step guide to build QGIS with Oracle support on a linux (ubuntu) installation
