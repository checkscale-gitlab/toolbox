# Install Oracle Instant Client

If you don't have any Oracle Database Installation, you will need the Oracle Instant Client (basic + sdk).

> After one first try, I was not able to use a 19.3 Oracle Instant Client.
I changed my mind and used a 12.2 Oracle Instant Client.
BUT be carefull, if you already used the 'make' command for GDAL, you need to run 'make clean' before trying the other Oracle client.


- **Download the .rpm files** ( https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html )
```
oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm
```


- **Convert it in .deb packages** using ALIEN:
```
alien oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
alien oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm
```

- **Install Oracle instant client and tools** using GDEBI or in command line:
```
sudo dpkg -i oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.deb
sudo dpkg -i oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.deb
```

- **Set environment variables** in the terminal:
```
export ORACLE_HOME=/usr/lib/oracle/12.2/client64
export LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
export PATH=$PATH:$ORACLE_HOME/bin
```

  OR, add these env. variables for all users in the `/etc/profile.d/oracle.sh` (create oracle.sh if needed) . Here is an example of `/etc/environment` file with 12.2 client:
  ```
  export ORACLE_HOME=/usr/lib/oracle/12.2/client64
  export PATH=$PATH:$ORACLE_HOME/bin
  ```

  The `LD_LIBRARY_PATH` should not be added to the `/etc/environment` file but in the `/etc/ld.so.conf.d`:
  ```
  cd /etc/ld.so.conf.d
  sudo vi oracle-instantclient.conf
  ```
  and add `/usr/lib/oracle/12.2/client64/lib`
  Then, reload environement variable:
  ```
  ldconfig
  ```

# Install dependencies

- **Update APT-GET cache**:
```
sudo apt-get update
```

- **Dependencies installation**: check official qgis intallation doc for your Ubuntu distribution : https://htmlpreview.github.io/?https://raw.github.com/qgis/QGIS/master/doc/INSTALL.html#toc3

  for Ubuntu bionic (linux mint 19.1-Tessa):
```
sudo apt install bison ca-certificates ccache cmake cmake-curses-gui dh-python doxygen expect flex gdal-bin git graphviz grass-dev libexiv2-dev libexpat1-dev libfcgi-dev libgdal-dev libgeos-dev libgsl-dev libosgearth-dev libpq-dev libproj-dev libqca-qt5-2-dev libqca-qt5-2-plugins libqt5opengl5-dev libqt5scintilla2-dev libqt5serialport5-dev libqt5sql5-sqlite libqt5svg5-dev libqt5webkit5-dev libqt5xmlpatterns5-dev libqwt-qt5-dev libspatialindex-dev libspatialite-dev libsqlite3-dev libsqlite3-mod-spatialite libyaml-tiny-perl libzip-dev lighttpd locales ninja-build ocl-icd-opencl-dev opencl-headers pkg-config poppler-utils pyqt5-dev pyqt5-dev-tools pyqt5.qsci-dev python-autopep8 python3-all-dev python3-dateutil python3-dev python3-future python3-gdal python3-httplib2 python3-jinja2 python3-markupsafe python3-mock python3-nose2 python3-owslib python3-plotly python3-psycopg2 python3-pygments python3-pyproj python3-pyqt5 python3-pyqt5.qsci python3-pyqt5.qtsql python3-pyqt5.qtsvg python3-pyqt5.qtwebkit python3-requests python3-sip python3-sip-dev python3-six python3-termcolor python3-tz python3-yaml qt3d-assimpsceneimport-plugin qt3d-defaultgeometryloader-plugin qt3d-gltfsceneio-plugin qt3d-scene2d-plugin qt3d5-dev qt5-default qt5keychain-dev qtbase5-dev qtbase5-private-dev qtpositioning5-dev qttools5-dev qttools5-dev-tools saga spawn-fcgi txt2tags xauth xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable xvfb
```

# Build GDAL with Oracle drivers  

- **Clone GDAL GIT** repo:
```
git clone https://github.com/OSGeo/gdal.git
```


- **List Releases** available:
```
git branch -a
```


- **Git checkout to set the release version**:

  - For 3.4 QGIS:
  ```
  git checkout release/2.4.2
  ```

  - For 3.10 QGIS:
  ```
  git checkout release/3.0
  ```

- Create **Symbolic link for Oracle sdk include folder**. They are both pointing on the same folder but GDAL need the 'xxx/sdk/include' and other installers need 'xxx/include'. You can try with only the first one.
```
sudo mkdir $ORACLE_HOME/sdk
sudo ln -s /usr/include/oracle/12.2/client64 $ORACLE_HOME/sdk/include
sudo ln -s /usr/include/oracle/12.2/client64 $ORACLE_HOME/include
```

- In the `(...)/gdal/gdal` repository, run the **Configure** command:
```
./configure \
  --with-libtiff=internal \
  --with-geotiff=internal \
  --with-jpeg=internal \
  --with-gif=internal \
  --with-python=/usr/bin/python3 \
  --with-openjpeg=yes \
  --with-oci=yes \
  --with-oci-include=/usr/lib/oracle/12.2/client64/include \
  --with-oci-lib=/usr/lib/oracle/12.2/client64/lib \
  LDFLAGS="-L/usr/lib/oracle/12.2/client64/lib"\
  LD_SHADE="gcc -shared"
```

- **Note about the Python version**:

  Be carefull with the PYTHON environnment: GDAL binaries are not often compatible with earlier python version.
  You should consider an "older" but stable (and supported by GDAL) python version.
  Personnal advice: avoid using Anaconda/Conda/IPython version because of the multiple (hidden) environment variable. KEEP IT SIMPLE and use basic python ;-)

- After the `./configure (...)`, **check in the list of format** if Oracle support is in the list. Or open the config.log:
  ```
  (...)
  configure:26334: checking for Oracle OCI headers in /usr/lib/oracle/12.2/client64/sdk/include
  configure:26367: g++ -c -DHAVE_AVX_AT_COMPILE_TIME -DHAVE_SSSE3_AT_COMPILE_TIME -DHAVE_SSE_AT_COMPILE_TIME -g -O2  -I/usr/lib/oracle/12.2/client64/rdbms/public -I/usr/lib/oracle/12.2/client64/rdbms/demo -I/usr/lib/oracle/12.2/client64/sdk/include conftest.cpp >&5
  configure:26367: $? = 0
  configure:26380: result: yes
  configure:26401: checking for Oracle OCI libraries in /usr/lib/oracle/12.2/client64/lib
  configure:26427: g++ -o conftest -DHAVE_AVX_AT_COMPILE_TIME -DHAVE_SSSE3_AT_COMPILE_TIME -DHAVE_SSE_AT_COMPILE_TIME -g -O2  -I/usr/lib/oracle/12.2/client64/rdbms/public -I/usr/lib/oracle/12.2/client64/rdbms/demo -I/usr/lib/oracle/12.2/client64/sdk/include -L/usr/lib/oracle/12.2/client64/lib conftest.cpp -lopenjp2 -L/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu/hdf5/serial -lnetcdf -lhdf5_hl -lhdf5 -lpthread -lsz -lz -ldl -lm -lcurl -L/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5 -lmfhdfalt -ldfalt -logdi -lpng -L/usr/lib/x86_64-linux-gnu -lpq -lz -lpthread -lm -lrt -ldl  -L/usr/lib/oracle/12.2/client64/lib -lclntsh >&5
  configure:26427: $? = 0
  configure:26431: result: yes
  configure:26475: checking if Oracle OCI version is >= 10.0.1
  configure:26495: result: yes
  configure:26498: checking for Oracle version >= 10.x to use -lnnz10 flag
  configure:26503: result: yes
  configure:26524: checking if Oracle support is enabled
  configure:26538: result: yes
  (...)
  ```


- Run **make**:
```
make
```

- **Check** if Oracle spatial support is activated: open the `GDALmake.opt` file and look at the Oracle Spatial Support section:
```
#
# Oracle Spatial Support
#
HAVE_OCI    = yes
OCI_INCLUDE = -I/usr/lib/oracle/12.2/client64/rdbms/public -I/usr/lib/oracle/12.2/client64/rdbms/demo -I/usr/lib/oracle/12.2/client64/sdk/include
```

- **Make install**:
```
sudo make install
```

- **Check** for GeoRaster support in the gdal `bin` repository:
```
cd /usr/local/bin/
gdalinfo --formats | grep GeoRaster
```

- **Check** for OCI support:
```
cd /usr/local/bin/
ogrinfo --formats | grep OCI
```

# Install QGIS

- **Clone Github QGIS repo**:
```
mkdir /media/bde/DATA/Soft_and_tools/QGIS
cd /media/bde/DATA/Soft_and_tools/QGIS
git clone https://github.com/qgis/QGIS.git
```

- **Choose the release** your are interested by:
```
cd QGIS
# MASTER branch
git checkout master
# R3.8 branch
# git checkout release-3_8
git config core.filemode false
```

- Run **CCMAKE**:
```
mkdir build
cd build
ccmake   -D CMAKE_INSTALL_PREFIX:PATH=${HOME}/apps/qgis-dev   -D CMAKE_BUILD_TYPE=Debug   -D WITH_ORACLE=TRUE   -D ORACLE_INCLUDEDIR=/usr/include/oracle/12.2/client64   -D ORACLE_LIBDIR=/usr/lib/oracle/12.2/client64/lib   -D GDAL_CONFIG=/usr/local/bin/gdal-config   -D GDAL_INCLUDE_DIR=/usr/local/include   -D GDAL_LIBRARY=/usr/local/lib/libgdal.so -D PYTHON_INCLUDE_PATH=/usr/include/python3.6  ..
```
Then press 'c' to configure and then 'g' to generate the makefile. **OR** use the **CMAKE** command:
```
cmake ..
```

- Run **MAKE**:
```
make
```

- **Make install**:
```
make install
```

- Create a **shell script for launching qgis**:
```
#!/bin/bash
echo "Start QGIS - dev"
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/lib/oracle/12.2/client64/lib:${HOME}/apps/qgis-dev/lib
~/apps/qgis-dev/bin/qgis
```

- BONUS:  **creating a shortcut** for QGIS-dev.

  Go in the `/usr/share/applications` folder and create a shortcut (.desktop file) as sudo.

  ```
  [Desktop Entry]
  Type=Application
  Name=QGIS Dev
  GenericName=Geographic Information System
  Icon=qgis
  TryExec=qgis
  Exec=/home/bde/Desktop/TOOLS/Software/qgis_START.sh
  Terminal=false
  StartupNotify=false
  Categories=Qt;Education;Science;Geography;
  MimeType=application/x-qgis-project;application/x-qgis-layer-settings;application/x-qgis-layer-definition;application/x-qgis-composer-template;image/tiff;image/jpeg;image/jp2;application/x-raster-aig;application/x-raster-ecw;application/x-raster-mrsid;application/x-mapinfo-mif;application/x-esri-shape;
  Keywords=map;globe;postgis;wms;wfs;ogc;osgeo;
  StartupWMClass=QGIS3
  Name[en_US]=QGIS Dev
  ```

  Then, in the start menu, type  *qgis*.

  On the icon, right clic > "Add to desktop". A new icon is deployed on the desktop. Then, move it where you need it.
  The advantage is that you don't need to keep the launch terminal opened.

- **Check** if the Oracle Spatial layer and Oracle GeoRaster layer are available in "Layer > Add layer"


# Uninstall GDAL

- Go in the build folder and run **make uninstall**:
```
cd /media/bde/DATA/Soft_and_tools/QGIS/QGIS/build
sudo make uninstall
```

- Then run a **make clean** to remove any intermediate or output files from your source / build tree:
```
make clean
```

# Uninstall QGIS

- Go in the build folder and run **make uninstall**:
```
cd /media/bde/DATA/Soft_and_tools/QGIS/QGIS/build
sudo make uninstall
```

- Then run a **make clean** to remove any intermediate or output files from your source / build tree:
```
make clean
```
