#!/bin/bash
SHAPE_FOLDER=/my/working/folder

IP_ADDRESS=localhost
PORT=1521
SERVICE_NAME=PLAYGROUND_PDB
USER=SCOTT
PASSWORD=TIGER

for g in $(ls $SHAPE_FOLDER/*.shp)
	do 
	echo 
	FULL_FILE_NAME=${g%.*}
	FILE_NAME=${g##*/}
		echo "FILE: "$FILE_NAME
		echo "OGRINFO:"
		ogrinfo -q $FILE_NAME
		ogr2ogr -f "OCI" OCI:"$USER/$PASSWORD@$IP_ADDRESS:$PORT/$SERVICE_NAME:" $FILE_NAME -lco DIM=2 -update -lco INDEX=NO
	 done	
pause
