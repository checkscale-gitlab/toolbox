REM Import all the Shafile(s) from a directory to a POSTGIS Database:
REM No spatial indexes,
REM Forcing 2d,
REM Updating existing table,
REM Setting geometry col name

@ECHO OFF
set HOST=localhost
set PORT=5432
set USER=postgres
set DBNAME=spatial_playground
set PASSWORD=postgres

for /r %%i in (*.shp) do (
	echo.
	echo FILE: %%~nxi
	echo OGRINFO: 
	ogrinfo -q %%~nxi
	ogr2ogr -f "PostgreSQL" PG:"host=%HOST% dbname=%DBNAME% user=%USER% port=%PORT% password=%PASSWORD%" %%~nxi -lco DIM=2 -update -append -lco INDEX=NO -lco GEOMETRY_NAME=geometry
)

pause