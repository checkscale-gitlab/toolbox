#!/bin/bash

echo "Load NATURAL EARTH data"

ogrinfo natural_earth_vector.sqlite 

ogr2ogr \
	 -progress \ 
     -f Postgresql \
     -s_srs EPSG:4326 \
     -t_srs EPSG:4326 \
     PG:"dbname=NATURAL_EARTH host=localhost port=5432 user=postgres password=postgres" \
     -lco DIM=2 \
     -nln geo_area \
     -overwrite \
     -sql "select name as name, geometry as geometry from ne_10m_lakes"\
     -dialect SQLITE \
     natural_earth_vector.sqlite

