REM Import all the Shafile(s) from a directory to an Oracle Spatial Database:
REM No spatial indexes,
REM Forcing 2d,
REM Updating existing table

@ECHO OFF
set IP_ADDRESS=localhost
set PORT=1521
set SERVICE_NAME=PLAYNGROUND_PDB
set USER=SCOTT
set PASSWORD=TIGER

for /r %%i in ("shp\*.shp") do (
	echo.
	echo FILE: %%~nxi
	echo OGRINFO: 
	ogrinfo -q %%~nxi
	ogr2ogr -f "OCI" OCI:"%USER%/%PASSWORD%@%IP_ADDRESS%:%PORT%/%SERVICE_NAME%:" %%~nxi -lco DIM=2 -update -lco INDEX=NO
)

pause