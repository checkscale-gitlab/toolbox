# Using QGIS from Conda

## Create a Conda environment

We create a dedicated Conda env. with additional tools/lib like _jupyter_, _spyder_ and _pandas_.
Note that we use _conda-forge_ repositories.

```bash
conda create -n qgis_dev qgis jupyter spyder pandas -c conda-forge
```

## Activate/deactivate conda environment
Activate:
```bash
conda activate qgis_dev
```

Deactivate:
```bash
conda deactivate
```
## Get QGIS version

conda-forge directories do not contain the latest version of QGIS but a slightly older version. To get the version of QGIS installed, type:

```bash
qgis --version
```


## Update QGIS in conda environment
```bash
conda update qgis -c conda-forge
```

## Run qgis
To launch this QGIS setup from your terminal:
```bash
conda activate qgis_dev
qgis
```
If you want to run QGIS in __debug mode__, use:
```bash
conda activate qgis_dev
GIS_DEBUG=9 QGIS_LOG_FILE=/tmp/qgis.log qgis
```
