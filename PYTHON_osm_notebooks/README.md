# Toolbox of jupyter notebooks using OpenStreetMap (osm) services

## Geocoding
- ```Geocoding.ipynb:```
Example of geocoding using __nominatim__ to geocode addresses from a spreadsheet.

## Overpass API
- ```Overpass_api_1_shelters.ipynb:```
Containing an example of query to the Overpass API to get geographical data from OSM. 
In this example, we request "shelters" to find ideal places to pitch your tent and bivouac in nature.
These data are received in __json__ and are loaded locally in a pandas __geodataframe__.
Once we have it, we plot the different category of shelters using __folium__.

- ```Overpass_api_2_towns.ipynb:```
In this example we query maltese cities and other kind of places to the Overpass API.
These data are received in __json__ and are loaded locally in a pandas __geodataframe__.
Once we have it, we plot the different category of shelters using __folium__.
